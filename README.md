# Belajar NodeJS dan MongoDB  

## Initial NodeJS
> npm init

## Express  
https://expressjs.com/
> npm install --save express

## Nodemon
https://www.npmjs.com/package/nodemon
> npm install -g nodemon  
> nodemon

## Express Handlebars
https://github.com/ericf/express-handlebars  
> npm install express-handlebars  
> npm install express-handlebars --save
> npm i -D handlebars@4.5.0

## Mongoose
https://mongoosejs.com/
> npm install mongoose  
> npm install --save mongoose

## Body Parser
https://www.npmjs.com/package/body-parser
> npm install body-parser  
> npm install --save body-parser

## Method Override
https://github.com/expressjs/method-override
> npm install method-override  
> npm install --save method-override

## Express Session
https://www.npmjs.com/package/express-session
> npm install express-session
> npm install express-session --save

## Connect Flash
https://www.npmjs.com/package/connect-flash
> npm install connect-flash
> npm install connect-flash --save

## Passport
http://www.passportjs.org/
> npm install passport
> npm install --save passport 
>> npm install --save passport-local 

## BcryptJS
https://www.npmjs.com/package/bcryptjs
> npm install bcryptjs
> npm install --save bcryptjs

## Plugin
### EJS
https://github.com/mde/ejs  
> npm install ejs
> npm install --save ejs
### Pug
https://github.com/pugjs/pug/
> npm install pug
> npm install --save pug
> npm install -g pug-cli
> pug --help